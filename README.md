# instantlinux-web
Official InstantLinux website.

## Preview
Click [here](https://instantcodee.github.io/instantlinux-web/) for a preview of this website.
Or host it for yourself:
1. Clone this repository
2. Do `npm install` to install all dependencies (you need `npm` installed for this)
3. Do `ng serve --open` to start the server (this opens your default browser automatically).

## Known issues
1. We know that our website is a little bit laggy in WebKit based browsers (like Vivaldi or Chrome) but it work perfectly in Firefox Quantum.

##### More coming soon...
